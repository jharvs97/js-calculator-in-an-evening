
# Simple calculator web-app

## How to run
Serve the files in the app directory by running your prefered http web server; for example

```
> cd app
> python -m http.server
```

If you wanting to add changes and see them appear in the browser, firstly

```
> cd ..
> npm install
```

and then

```
> node ./hotloader.js
```

finally, refresh the browser to connect to the hotloader