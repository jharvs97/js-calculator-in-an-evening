let app = {}
let expr = ""

app.attach = () => {
    app.listenForChanges()

    const $result = document.querySelector('.result')

    document.querySelectorAll(".btn").forEach($btn => {
        const command = $btn.innerText
        $btn.addEventListener("click", (evt) => {
            if (command == '=') {
                const validRegex = /\d+[\*\-\/\+]\d+/
                const match = expr.match(validRegex)
                if (match && match[0].length == expr.length) {
                    const opRegex = /[\*\-\/\+]/
                    const split = expr.split(opRegex)
                    const op = expr[expr.search(opRegex)]
                    const num1 = Number.parseInt(split[0])
                    const num2 = Number.parseInt(split[1])
                    let result = 0
                    switch (op) {
                        case '/': result = num1 / num2; break
                        case '+': result = num1 + num2; break
                        case '-': result = num1 - num2; break
                        case '*': result = num1 * num2; break
                    }
                    console.log(result)
                    $result.innerHTML = result
                }
                else{
                    $result.innerHTML = "Invalid"
                }

                expr = ""

            }
            else if (command == 'C') {
                expr = ""
                $result.innerHTML = expr
            }
            else {
                expr += command
                $result.innerHTML = expr

            }
        })
    })
}

app.listenForChanges = () => {
    const ws = new WebSocket("ws://localhost:8080");
    
    ws.onopen = () => {
        console.log("Websocket open")
        
        ws.onclose = () => {
            setTimeout(() => {
                connectSocket()
            }, 2000)
        }

        ws.onerror = () => {
            console.log("Websocket connection error")
        }

        ws.onmessage = (evt) => {
            const payload = JSON.parse(evt.data)

            if (payload.newChanges) {
                location.reload()
            }
        }
    }
}