const WebSocket = require('ws')
const fs = require('fs')

console.log("Starting hotloader")
const wss = new WebSocket.Server({port: 8080})

let client = null
wss.on('connection', (socket, message) => {
    client = socket;
    console.log("Client connected")
})

fs.watch('./app', {recursive: true}, (e, filename) => {
    if (/.(js)|(html)|(css)/.test(filename) && client && client.readyState == WebSocket.OPEN) {
        console.log("Changes detected, notifying client")
        client.send(JSON.stringify({newChanges:true}))
    }
})